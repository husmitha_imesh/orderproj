const crypto = require('crypto');
const {key} = require('../Config/PasswordConst');
const VenderModel = require('../Models/Vendor');


  function decyp(de_password){

    // const user = await VenderModel.findOne({ _id: req.body.id });

    // const storedPassword = user.password;

    let dicipher = crypto.createDecipher('aes-256-cbc', key);
    let decrypted = dicipher.update(de_password, 'hex', 'utf8');
    decrypted += dicipher.final('utf8');
    return decrypted;
}

module.exports = {decyp}