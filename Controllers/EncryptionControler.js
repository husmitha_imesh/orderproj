const crypto = require('crypto');
const {key} = require('../Config/PasswordConst');

 function authfcn(password){
    

    let ciper = crypto.createCipher('aes-256-cbc', key);
    let encryptec = ciper.update(password, 'utf8', 'hex');
    encryptec += ciper.final('hex');
    return encryptec;

}
module.exports = {authfcn}
