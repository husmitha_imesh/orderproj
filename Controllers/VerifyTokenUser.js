const jwt = require('jsonwebtoken');
const {TOKEN_SECRET} = require('../Config/TokenSec');


module. exports =  function(req, res, next){
    const token = req.header('Auth');
    if(!token){
        return res.status(400).send("Access Denied");
    }


    try{

        const verified = jwt.verify(token, TOKEN_SECRET);
        req.user = verified;
        next();

    }catch(err){
        res.status(400).send("Invalid token");
    }
}