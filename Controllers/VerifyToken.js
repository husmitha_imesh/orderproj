const jwt = require('jsonwebtoken');
const {TOKEN_SEC} = require('../Config/TokenSecVendor');

module. exports =  function(req, res, next){
    const token = req.header('Authentication');
    if(!token){
        return res.status(400).send("Access Denied");
    }


    try{

        const verified = jwt.verify(token, TOKEN_SEC);

        // const _id = verified.id;
        req.user = verified;
        next();
        // return _id;

    }catch(err){
        res.status(400).send("Invalid token");
    }
}