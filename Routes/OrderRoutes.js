const express = require('express');
const router = express.Router();
const verify = require('../Controllers/VerifyTokenUser');
const verifyVen = require('../Controllers/VerifyToken');
const ProductModel = require('../Models/Product');
const OrderModel = require('../Models/Order');
const StoreModel = require('../Models/Store');
const CustomerModel = require('../Models/User');
const VendorModel = require('../Models/Vendor');


/**
 * @swagger
 * /order/addorder:
 *   post:
 *      tags:
 *       - Orders
 *      description: post request for add orders
 *      produces:
 *       - application/json
 *      parameters:
 *       - name: Auth
 *         description: 
 *         in: header
 *         required: true
 *         type: string
 *       - name: Body
 *         description: User object
 *         in: body
 *         required: true
 *         schema:
 *           $ref: '#/definitions/loginRequest'
 *      responses:
 *          200:
 *           description: A successfully add orders
 * definitions:
 *   loginRequest:
 *     properties:
 *       Storename:
 *         type: string
 *       id:
 *         type: string
 *       prod:
 *         type: string
 *       qty:
 *         type: string
 *       id1:
 *         type: string
 *       prod1:
 *         type: string
 *       qty1:
 *         type: string
 *       id2:
 *         type: string
 *       prod2:
 *         type: string
 *       qty2:
 *         type: string
 *
 */
router.post('/addorder', verify, async(req, res) => {

    const upcus = await CustomerModel.findOne({ _id: req.user._id});

    if(upcus.isDeleted == true){
      return res.status(400).send('cannot add stores');
    }

    const storeID = await StoreModel.findOne({Storename: req.body.Storename});

    // const products = await ProductModel.findOne({_id: req.body._id})


    // if(!products){
    //     return res.status(400).send('No product available');
    // }
    
    // const prodquan = parseInt(products.quantity);
    // const qt = parseInt(req.body.qty);

    // if(prodquan < qt){
    //     return res.status(400).send("Not enough products");
    // }


    // let array=[];

    // array.forEach(products => {
    //     array.push(products);
        
    // });

    // console.log(array);
    
    const cusId = upcus._id;
    const SId = storeID._id;
    const pid = storeID.vendorID;
  


  const xp =  [{
    prodId: req.body.id,
    prodName: req.body.prod,
    quant: req.body.qty
  },
   {
    prodId: req.body.id1,
    prodName: req.body.prod1,
    quant: req.body.qty1
   }, 
    {
      prodId: req.body.id2,
      prodName: req.body.prod2,
      quant: req.body.qty2
    }]


    const ab = JSON.stringify(xp);
    //example of add values for prodId: 
    //"[{"prodId":"order1#2","prodName":"pizza","quant":"5"},
    //{"prodId":"order1#3","prodName":"coca cola","quant":"10"},
    //{"prodId":"order1#4","prodName":"bread"}]"
    
    console.log(ab);

    const addprod = new OrderModel({
        customerId: cusId,
        storeId: SId,
        productId: ab,
        vendorId: pid,
        qty: 23,
        ordertotal: 6540,
        placedate: new Date(),

        
      })
      // const x = JSON.parse.toString(addprod);
      
      try{
        

        const savestore = await addprod.save();

        res.status(200).send("Added Order");

      }catch(err){
        res.json(err);
      }
})





/**
 * @swagger
 * /order/deleteorder/:
 *   delete:
 *      tags:
 *       - Orders
 *      description: delete request for delete orders
 *      produces:
 *       - application/json
 *      parameters:
 *       - name: Auth
 *         description: 
 *         in: header
 *         required: true
 *         type: string
 *       - name: Body
 *         description: User object
 *         in: body
 *         required: true
 *      responses:
 *          200:
 *           description: A successfully delete orders
 *
 */
router.delete('/vendor/deleteorder/', verifyVen, async(req, res) => {

    // const upvendor1 = await CustomerModel.findOne({ _id: req.user._id});

    // if(upvendor1.isDeleted == true){
    //   return res.status(400).send('cannot find vendor');
    // }

    const venname1 = await VendorModel.findOne({ _id: req.user._id  });

      
    //   const storename = await StoreModel.findOne({ Storename: req.body.Storename });
    // if (!storename) {
    //   return res.status(400).send('store not exist');
    // }
  
    const storename12 = await OrderModel.findOne({ vendorId: req.body.vendorId, _id: req.body._id});
    if (storename12.Status !== "Accepted order") {
      return res.status(400).send('Not accepted therefore cannot delete order');
    }


 
    try{
        const updatevendor = await storename12.updateOne({Status: "deleted order"});
  
    
        res.status(200).send("Successfully deleted order!");
    

      
  }catch(err){
      res.json(err);
  }
   
  })
  

router.post('/vendor/addorder', verifyVen, async(req, res) => {

  const venname = await VendorModel.findOne({ _id: req.user._id });

  console.log(venname);

  const orderselected = await OrderModel.findOne({ vendorId: req.body.vendorId, _id: req.body._id });
  try{

  const ven = venname._id;
  const ord = orderselected.vendorId;

  // if(ven !== ord){
  //   return res.status(400).send("vendor not found");
  // }
  console.log(venname._id);
  console.log(orderselected.vendorId);
    
  const updatevendor = await orderselected.updateOne({Status: "Accepted order"});

    
    res.status(200).send("Added vendor status update");

  }catch(err){
    res.json(err);
  }

})


module.exports = router;


