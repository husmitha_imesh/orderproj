const express = require('express');
const router = express.Router();
// const crypto = require('crypto');
const jwt = require('jsonwebtoken');

const {authfcn} = require('../Controllers/EncryptionControler');
const {decyp} = require('../Controllers/DecryptionController');
const verify = require('../Controllers/VerifyTokenUser');

const {TOKEN_SECRET} = require('../Config/TokenSec');


const UserModel = require('../Models/User');


/**
 * @swagger
 * /user/adduser:
 *   post:
 *      tags:
 *        - User
 *      description: post request for add users
 *      produces:
 *       - application/json
 *      parameters:
 *       - name: Body
 *         description: User object
 *         in: body
 *         required: true
 *         schema:
 *           $ref: '#/definitions/loginRequest'
 *      responses:
 *          200:
 *           description: A successful token return for add user
 * definitions:
 *   loginRequest:
 *     properties:
 *       username:
 *         type: string
 *       email:
 *         type: string
 *       password:
 *         type: string
 *       address:
 *         type: string
 *
 */
router.post('/adduser', async(req, res) => {
    const idexist = await UserModel.findOne({ email: req.body.email, isDeleted: false});
  if (idexist) {
    return res.status(400).send('Email already exist');
  }


  const encrypted = authfcn(req.body.password);

  const adduser = new UserModel({
    username: req.body.username,
    email: req.body.email,
    password: encrypted,
    address: req.body.address,
    
  })



  try{

        const saveVendor = await adduser.save();

        const token1 = jwt.sign({ _id: adduser._id, email: adduser.email }, TOKEN_SECRET);
    res.status(200).send(token1);

        
  }catch(err){

    res.json(err);
  }

})




/**
 * @swagger
 * /user/deleteUser:
 *   delete:
 *      tags:
 *        - User
 *      description: delete request for delete vendors
 *      produces:
 *       - application/json
 *      parameters:
 *       - name: Auth
 *         description: 
 *         in: header
 *         required: true
 *         type: string
 *       - name: Body
 *         description: User object
 *         in: body
 *         required: true
 *      responses:
 *          200:
 *           description: A successful delete user
 *
 */
router.delete('/deleteUser', verify, async(req, res) => {

    const delVendor = await UserModel.findOne({ _id: req.user._id});
  
    const updatevendor = await delVendor.updateOne({isDeleted: true});
    
    try{
      
  
        // console.log(_id);
  
  
        // console.log(isDeleted);
  
        if(delVendor.isDeleted == true){
          res.status(200).send("Successfully deleted!");
        }
  
       
        
    }catch(err){
        res.json(err);
    }
    
    
        
    })
  




/**
 * @swagger
 * /user/edituser/:
 *   put:
 *      tags:
 *        - User
 *      description: put request for edit users
 *      produces:
 *       - application/json
 *      parameters:
 *       - name: Auth
 *         description: 
 *         in: header
 *         required: true
 *         type: string
 *       - name: Body
 *         description: User object
 *         in: body
 *         required: true
 *         schema:
 *           $ref: '#/definitions/loginRequest'
 *      responses:
 *          200:
 *           description: A successfully edit users
 * definitions:
 *   loginRequest:
 *     properties:
 *       username:
 *         type: string
 *       email:
 *         type: string
 *       password:
 *         type: string
 *       address:
 *         type: string
 *
 */
router.put('/edituser/', verify, async (req, res) => {

    const upuser = await UserModel.findOne({ _id: req.user._id});

    if(upuser.isDeleted == true){
      return res.status(400).send('cannot update');
    }

    try{
        const updateduser = await UserModel.updateOne({_id: req.user._id}, 
            {$set: {username: req.body.username, address: req.body.address}});
            
            res.status(200).send("updated User");
    }catch(err){
        res.json(err);
    }
});



  
/**
 * @swagger
 * /user/login:
 *   post:
 *      tags:
 *        - User
 *      description: post request for login users
 *      produces:
 *       - application/json
 *      parameters:
 *       - name: Body
 *         description: User object
 *         in: body
 *         required: true
 *         schema:
 *           $ref: '#/definitions/loginRequest'
 *      responses:
 *          200:
 *           description: A successful token return for login
 * definitions:
 *   loginRequest:
 *     properties:
 *          email:
 *            type: string
 *          password:
 *            type: string
 *
 */

router.post('/login', async(req, res) => {

    const user = await UserModel.findOne({ email: req.body.email, isDeleted: false });
    if (!user) {
      return res.status(400).send('Email not found');
    }
  
    const storedPw = decyp(user.password);
  
    // const decrypting = decryption.decrypted;
  
  
    if (storedPw !== req.body.password) {
      return res.status(400).send('invalid paswword');
    }
  
    try{
  
      const tok = jwt.sign({ _id: user._id, email: user.email }, TOKEN_SECRET);
      res.status(200).send(tok);
  
      // res.status(200).send("Succesfully login");
  
    }catch(err){
      res.json(err);
    }
  
  
  
  })
  


module.exports = router;