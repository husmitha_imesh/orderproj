const express = require('express');
const router = express.Router();


const verify = require('../Controllers/VerifyToken');

const ProductModel = require('../Models/Product');
const VendorModel = require('../Models/Vendor');
const StoreModel = require('../Models/Store');



/**
 * @swagger
 * /product/addproduct:
 *   post:
 *      tags:
 *       - Products
 *      description: post request for add products
 *      produces:
 *       - application/json
 *      parameters:
 *       - name: Authentication
 *         description: 
 *         in: header
 *         required: true
 *         type: string
 *       - name: Body
 *         description: User object
 *         in: body
 *         required: true
 *         schema:
 *           $ref: '#/definitions/loginRequest'
 *      responses:
 *          200:
 *           description: A successfully add products
 * definitions:
 *   loginRequest:
 *     properties:
 *       Productname:
 *         type: string
 *       description:
 *         type: string
 *       category:
 *         type: string
 *       quantity:
 *         type: string
 *       price:
 *         type: string
 *
 */
router.post('/addproduct', verify, async(req, res) => {


    const upvendor = await VendorModel.findOne({ _id: req.user._id});

    if(upvendor.isDeleted == true){
      return res.status(400).send('cannot add stores');
    }

    const storeID = await StoreModel.findOne({Storename: req.body.Storename});

    

    const vId = upvendor._id;
    const SId = storeID._id;
    

    const addprod = new ProductModel({
        Productname: req.body.Productname,
        description: req.body.description,
        category: req.body.category,
        quantity: req.body.quantity,
        price: req.body.price,
        storeId: SId,
        vendorId: vId,
      })

      try{

        const savestore = await addprod.save();

        res.status(200).send("Added product");

      }catch(err){
        res.json(err);
      }
})





/**
 * @swagger
 * /product/editproduct/:
 *   put:
 *      tags:
 *       - Products
 *      description: put request for edit products
 *      produces:
 *       - application/json
 *      parameters:
 *       - name: Authentication
 *         description: 
 *         in: header
 *         required: true
 *         type: string
 *       - name: Body
 *         description: User object
 *         in: body
 *         required: true
 *         schema:
 *           $ref: '#/definitions/loginRequest'
 *      responses:
 *          200:
 *           description: A successfully edit product
 * definitions:
 *   loginRequest:
 *     properties:
 *        Productname:
 *         type: string
 *        description:
 *         type: string
 *        category:
 *         type: string
 *        quantity:
 *         type: string
 *        price:
 *         type: string
 *
 */
router.put('/editproduct/', verify, async (req, res) => {

    const upvendor2 = await VendorModel.findOne({ _id: req.user._id});
    if(upvendor2.isDeleted == true){
      return res.status(400).send('cannot add stores');
    }

//   const emailed = req.user.email;
  
//   const stre = await StoreModel.findOne({ Storename: req.body.Storename });
//   if (!stre) {
//     return res.status(400).send('store not exist');
//   }


  const stre = await ProductModel.findOne({ Productname: req.body.Productname});
  if (!stre) {
    return res.status(400).send('store not exist');
  }
  
  
//   if(vendorEmail !== emailed){
//     res.status(400).send("Email does not matched");
//   }

    try{
        const updateVendor = await ProductModel.updateOne({ Productname: req.body.Productname}, 
            {$set: {description: req.body.description, categorey: req.body.categorey, quantity: req.body.quantity}});
            
            res.status(200).send("updated Product");
    }catch(err){
        res.json(err);
    }
});




/**
 * @swagger
 * /product/deleteProduct/:
 *   delete:
 *      tags:
 *       - Products
 *      description: delete request for delete products
 *      produces:
 *       - application/json
 *      parameters:
 *       - name: Authentication
 *         description: 
 *         in: header
 *         required: true
 *         type: string
 *       - name: Body
 *         description: User object
 *         in: body
 *         required: true
 *      responses:
 *          200:
 *           description: A successfully delete products
 *
 */
router.delete('/deleteProduct/', verify, async(req, res) => {

    // const emailed = req.user.email;

    const upvendor1 = await VendorModel.findOne({ _id: req.user._id});

    if(upvendor1.isDeleted == true){
      return res.status(400).send('cannot find vendor');
    }
    // console.log(emailed);
  
      const storename = await StoreModel.findOne({ Storename: req.body.Storename });
    if (!storename) {
      return res.status(400).send('store not exist');
    }
  
    // console.log(nameexist.email);
  
    // if(vendorEmail !== emailed){
    //   res.status(400).send("Email does not matched");
    // }

    const storename12 = await ProductModel.findOne({ Productname: req.body.Productname });
    if (!storename12) {
      return res.status(400).send('store not exist');
    }

    
    try{
        const updatevendor = await storename12.updateOne({isDeleted: true});
  
    if(storename12.isDeleted == true){
        res.status(200).send("Successfully deleted product!");
      }

    //   const delVendor = await StoreModel.deleteOne({ Storename: req.body.Storename});
    //   res.status(200).send("Successfully deleted!");
      
  }catch(err){
      res.json(err);
  }
  
  
      
  })
  


  
/**
* @swagger
* /product/getProduct/:
*   get:
*     tags:
*       - Products
*     description: Get products 
*     produces:
*       - application/json
*     parameters:
*       - name: Authentication
*         description: 
*         in: header
*         required: true
*         type: string
*       - name: Body
*         description: Admin object
*         in: body
*         required: true
*     responses:
*       200:
*         description: Admin object
*         
*/
router.get('/getProduct/', verify, async(req, res) => {

  
  const upvendorget = await VendorModel.findOne({ _id: req.user._id});

  if(upvendorget.isDeleted == true){
    return res.status(400).send('cannot find vendor');
  }


  const storename = await StoreModel.findOne({ _id: req.body._id });
  if (!storename) {
    return res.status(400).send('store not exist');
  }


  const getproduct = await ProductModel.find();

  return res.status(200).send(getproduct);

})





module.exports = router;