const express = require('express');
const router = express.Router();

const jwt = require('jsonwebtoken');
const {authfcn} = require('../Controllers/EncryptionControler');
const {decyp} = require('../Controllers/DecryptionController');
const verify = require('../Controllers/VerifyToken');
const {TOKEN_SEC} = require('../Config/TokenSecVendor');

const VendorModel = require('../Models/Vendor');



/**
 * @swagger
 * /vendor/addvendor:
 *   post:
 *      tags:
 *        - Vendor
 *      description: post request for add vendors
 *      produces:
 *       - application/json
 *      parameters:
 *       - name: Body
 *         description: User object
 *         in: body
 *         required: true
 *         schema:
 *           $ref: '#/definitions/loginRequest'
 *      responses:
 *          200:
 *           description: A successful token return for add vendor
 * definitions:
 *   loginRequest:
 *     properties:
 *       username:
 *         type: string
 *       email:
 *         type: string
 *       password:
 *         type: string
 *       address:
 *         type: string
 *
 */
router.post('/addvendor', async(req, res) => {
    const idexist = await VendorModel.findOne({ email: req.body.email, isDeleted: false});
  if (idexist) {
    return res.status(400).send('Email already exist');
  }


  const encrypted = authfcn(req.body.password);

  const addvendor = new VendorModel({
    username: req.body.username,
    email: req.body.email,
    password: encrypted,
    address: req.body.address,
    
  })



  try{

        const saveVendor = await addvendor.save();

        const token1 = jwt.sign({ _id: addvendor._id, email: addvendor.email }, TOKEN_SEC);
    res.status(200).send(token1);

        
  }catch(err){

    res.json(err);
  }

})



/**
 * @swagger
 * /vendor/deleteVendor:
 *   delete:
 *      tags:
 *        - Vendor
 *      description: delete request for delete vendors
 *      produces:
 *       - application/json
 *      parameters:
 *       - name: Authentication
 *         description: 
 *         in: header
 *         required: true
 *         type: string
 *       - name: Body
 *         description: User object
 *         in: body
 *         required: false
 *      responses:
 *          200:
 *           description: delete vendor
 *
 */
router.delete('/deleteVendor', verify, async(req, res) => {

  const delVendor = await VendorModel.findOne({ _id: req.user._id});

  
  try{
    
    const updatevendor = await delVendor.updateOne({isDeleted: true});

      // console.log(_id);


      // console.log(isDeleted);

      if(delVendor.isDeleted == true){
        res.status(200).send("Successfully deleted!");
      }

     
      
  }catch(err){
      res.json(err);
  }
  
  
      
  })






/**
 * @swagger
 * /vendor/editVendor/:
 *   put:
 *      tags:
 *        - Vendor
 *      description: put request for edit vendors
 *      produces:
 *       - application/json
 *      parameters:
 *       - name: Authentication
 *         description: 
 *         in: header
 *         required: true
 *         type: string
 *       - name: Body
 *         description: User object
 *         in: body
 *         required: true
 *         schema:
 *           $ref: '#/definitions/loginRequest'
 *      responses:
 *          200:
 *           description: edit vendor
 * definitions:
 *   loginRequest:
 *     properties:
 *       username:
 *         type: string
 *       email:
 *         type: string
 *       password:
 *         type: string
 *       address:
 *         type: string
 *
 */
  router.put('/editVendor/', verify, async (req, res) => {

    const upvendor = await VendorModel.findOne({ _id: req.user._id});

    if(upvendor.isDeleted == true){
      return res.status(400).send('cannot update');
    }

    try{
        const updatedVendor = await VendorModel.updateOne({_id: req.user._id}, 
            {$set: {username: req.body.username, address: req.body.address}});
            
            res.status(200).send("updated Vendor");
    }catch(err){
        res.json(err);
    }
});


  




/**
 * @swagger
 * /vendor/login:
 *   post:
 *      tags:
 *        - Vendor
 *      description: post request for login vendors
 *      produces:
 *       - application/json
 *      parameters:
 *       - name: Body
 *         description: User object
 *         in: body
 *         required: true
 *         schema:
 *           $ref: '#/definitions/loginRequest'
 *      responses:
 *          200:
 *           description: A successful token return for login
 * definitions:
 *   loginRequest:
 *     properties:
 *          email:
 *            type: string
 *          password:
 *            type: string
 *
 */

router.post('/login', async(req, res) => {

  const user = await VendorModel.findOne({ email: req.body.email, isDeleted: false });
  if (!user) {
    return res.status(400).send('Email not found');
  }

  const storedPw = decyp(user.password);

  // const decrypting = decryption.decrypted;


  if (storedPw !== req.body.password) {
    return res.status(400).send('invalid paswword');
  }

  try{

    const tok = jwt.sign({ _id: user._id, email: user.email }, TOKEN_SEC);
    res.status(200).send(tok);

    // res.status(200).send("Succesfully login");

  }catch(err){
    res.json(err);
  }



})


module.exports = router;