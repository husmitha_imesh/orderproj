const express = require('express');
const router = express.Router();
const StoreModel = require('../Models/Store');
const VendorModel = require('../Models/Vendor');

const verify = require('../Controllers/VerifyToken');


/**
 * @swagger
 * /store/addstores:
 *   post:
 *      tags:
 *        - Stores
 *      description: post request for add stores
 *      produces:
 *       - application/json
 *      parameters:
 *       - name: Authentication
 *         description: 
 *         in: header
 *         required: true
 *         type: string
 *       - name: Body
 *         description: User object
 *         in: body
 *         required: true
 *         schema:
 *           $ref: '#/definitions/loginRequest'
 *      responses:
 *          200:
 *           description: A successfully add stores
 * definitions:
 *   loginRequest:
 *     properties:
 *       Storename:
 *         type: string
 *       description:
 *         type: string
 *       categorey:
 *         type: string
 *       address:
 *         type: string
 *
 */
router.post('/addstores', verify, async(req, res) => {


    // const venexist = await VendorModel.findOne({ email: req.body.email, isDeleted: false});
    // if(!venexist){
    //     return res.status(400).send('There is no valid vendor');
    // }

    
    const upvendor = await VendorModel.findOne({ _id: req.user._id});

    if(upvendor.isDeleted == true){
      return res.status(400).send('cannot add stores');
    }

    const id_V = upvendor._id
    // const _id = req.user._id;

    const addstore = new StoreModel({
        Storename: req.body.Storename,
        description: req.body.description,
        categorey: req.body.categorey,
        vendorID: id_V,
      })

      try{

        const savestore = await addstore.save();

        res.status(200).send("Added store");

      }catch(err){
        res.json(err);
      }
})






/**
 * @swagger
 * /store/deleteStore/:
 *   delete:
 *      tags:
 *        - Stores
 *      description: delete request for delete stores
 *      produces:
 *       - application/json
 *      parameters:
 *       - name: Authentication
 *         description: 
 *         in: header
 *         required: true
 *         type: string
 *       - name: Body
 *         description: User object
 *         in: body
 *         required: true
 *      responses:
 *          200:
 *           description: A successfully delete stores
 *
 */
router.delete('/deleteStore/', verify, async(req, res) => {

    // const emailed = req.user.email;

    const upvendor1 = await VendorModel.findOne({ _id: req.user._id});

    if(upvendor1.isDeleted == true){
      return res.status(400).send('cannot find vendor');
    }
    // console.log(emailed);
  
      const storename = await StoreModel.findOne({ Storename: req.body.Storename });
    if (!storename) {
      return res.status(400).send('store not exist');
    }
  
    // console.log(nameexist.email);
  
    // if(vendorEmail !== emailed){
    //   res.status(400).send("Email does not matched");
    // }
    
    try{
        const updatevendor = await storename.updateOne({isDeleted: true});
  
    if(storename.isDeleted == true){
        res.status(200).send("Successfully deleted!");
      }

    //   const delVendor = await StoreModel.deleteOne({ Storename: req.body.Storename});
    //   res.status(200).send("Successfully deleted!");
      
  }catch(err){
      res.json(err);
  }
  
  
      
  })
  



  
/**
 * @swagger
 * /store/editStore/:
 *   put:
 *      tags:
 *        - Stores
 *      description: put request for edit stores
 *      produces:
 *       - application/json
 *      parameters:
 *       - name: Authentication
 *         description: 
 *         in: header
 *         required: true
 *         type: string
 *       - name: Body
 *         description: User object
 *         in: body
 *         required: true
 *         schema:
 *           $ref: '#/definitions/loginRequest'
 *      responses:
 *          200:
 *           description: A successfully edit stores
 * definitions:
 *   loginRequest:
 *     properties:
 *       Storename:
 *         type: string
 *       description:
 *         type: string
 *       categorey:
 *         type: string
 *       address:
 *         type: string
 *
 */
router.put('/editStore/', verify, async (req, res) => {

    const upvendor2 = await VendorModel.findOne({ _id: req.user._id});
    if(upvendor2.isDeleted == true){
      return res.status(400).send('cannot add stores');
    }

//   const emailed = req.user.email;
  
  const stre = await StoreModel.findOne({ Storename: req.body.Storename });
  if (!stre) {
    return res.status(400).send('store not exist');
  }
  
//   if(vendorEmail !== emailed){
//     res.status(400).send("Email does not matched");
//   }

    try{
        const updateVendor = await StoreModel.updateOne({ Storename: req.body.Storename}, 
            {$set: {description: req.body.description, categorey: req.body.categorey}});
            
            res.status(200).send("updated Store");
    }catch(err){
        res.json(err);
    }
});







/**
* @swagger
* /store/getStore/:
*   get:
*     tags:
*       - Stores
*     description: Get stores 
*     produces:
*       - application/json
*     parameters:
*       - name: Authentication
*         description: 
*         in: header
*         required: true
*         type: string
*       - name: Body
*         description: Admin object
*         in: body
*         required: true
*     responses:
*       200:
*         description: Admin object
*         
*/
router.get('/getStore/', verify, async(req, res) => {

  
  const upvendorget = await VendorModel.findOne({ _id: req.user._id});

  if(upvendorget.isDeleted == true){
    return res.status(400).send('cannot find vendor');
  }



  const getStore = await StoreModel.find();

  return res.status(200).send(getStore);

})



module.exports = router;