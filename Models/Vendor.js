const mongoose = require('mongoose');


const VendorSchema = mongoose.Schema({

    username: {
        type: String,
        required: true,
    },
    email: {
        type: String,
        required: true,
    },
    password: {
        type: String,
        required: true,
    },
    address: {
        type: String,
    
    },
    isDeleted: {
        type: Boolean,
        required: true,
        default: false
    }
})



module.exports = mongoose.model('Vendor', VendorSchema);