const mongoose = require('mongoose');


const productsordered = mongoose.Schema({
    prodId : {
        type: String,
    },
    prodName : {
        type: String,
    },
    quant : {
        type: String,
    }
})


const OrderSchema = mongoose.Schema({

    customerId : {
        type: String,
        required: true
    },
    storeId: {
        type: String,
        required: true
        
    },
    productId: {
        type: String
    },
    vendorId: {
        type: String,
       
    },

    qty: {
        type: String,
    },
    ordertotal: {
        type: String,
    },
    placedate: {
        type: String,

    },
    Status: {
        type: String,
       
        
    }
})



module.exports = mongoose.model('Order', OrderSchema);