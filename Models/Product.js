const mongoose = require('mongoose');


const ProductSchema = mongoose.Schema({

    Productname: {
        type: String,
        required: true,
    },
    description: {
        type: String,
        required: true,
    },
    category: {
        type: String,
        required: true,
    },
    quantity: {
        type: String,
        required: true,
    },
    price: {
        type: String,
        required: true,
    },
    storeId:{
        type: String,
        required: true
    },
    vendorId:{
        type: String,
        required: true
    },
    isDeleted: {
        type: Boolean,
        required: true,
        default: false
    }
})



module.exports = mongoose.model('Product', ProductSchema);