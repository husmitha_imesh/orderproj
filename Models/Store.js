const mongoose = require('mongoose');


const StoreSchema = mongoose.Schema({

    Storename: {
        type: String,
        required: true,
    },
    description: {
        type: String,
        required: true,
    },
    categorey:{
        type:String,
        required: true,
    },
    vendorID:{
        type: String,
        required: true,
    },
    isDeleted: {
        type: Boolean,
        required: true,
        default: false
    }

})



module.exports = mongoose.model('Store', StoreSchema);